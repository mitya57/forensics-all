#!/bin/bash

# search-for-new.sh -- searches for new files in Sid
#
# This file is part of the forensics-all.
#
# Copyright 2018 Joao Eriberto Mota Filho <eriberto@debian.org>
#
# You can use this program under the BSD-3-Clause conditions.

#################
### Variables ###
#################

[ -e variables ] || { echo "I can't find variables file. Aborting."; exit 1; }

source variables

[ "$1" = "--debug" ] && DEBUG=1
[ -z "$IGNORE1" ] && IGNORE1=" "
[ -z "$IGNORE2" ] && IGNORE2=" "
[ -z "$IGNORE3" ] && IGNORE3=" "
[ -z "$IGNORE4" ] && IGNORE4=" "


####################
### Main program ###
####################

### Initial check. We are in right place?

[ -e list-of-packages ] || { echo "I can't find list-of-packages file. Aborting."; exit 1; }

### Initial message

echo " "
echo "search-for-new.sh -- searches for new files in Sid"
echo " "
echo "Remember to run 'apt-get update' before to use this script."
echo " "
echo -e "You can edit the \e[5mvariables\e[25m file to exclude packages."
echo " "
echo "Use --debug to keep the build directory."
echo " "
echo "Press ENTER to continue or Ctrl-C to abort."
echo " "
read NOTHING

### Go!

[ -d build ] || mkdir build

# List of packages maintained by 'Debian Security Tools Team'
# from Debian package lists.
cat $LIST | grep "Maintainer: Debian Security Tools" -B6 | grep Package: | \
   cut -d" " -f2 | egrep -v \($NOTDEPEND\) | sort  > build/PACKAGES.txt

# List of packages in local list-of-packages file.
cat list-of-packages | cut -d" " -f1  | egrep '^[a-z0-9]' > build/list-of-packages

# Diff of previous lists
cat build/list-of-packages build/PACKAGES.txt | sort | uniq -u | \
   egrep -v "$IGNORE1" | egrep -v "$IGNORE2" | egrep -v "$IGNORE3" | \
   egrep -v "$IGNORE4" > build/diff

echo -e '---\n'

# Decision about what to show in screen
TEST=$(cat build/diff | egrep '[a-z]')

if [ "$TEST" ]
then
    echo -e "\nThe following packages are not present in Debian repository or in list of packages file:\n"
    for i in $(cat build/diff)
    do
       cat build/PACKAGES.txt | egrep "^$i$" > /dev/null && echo "$i (only in repo)"
       cat build/list-of-packages | egrep "^$i$" > /dev/null && echo "$i (only in local list)"
    done
else
    echo -e "\nNo differences detected between Debian repository and list of packages file.\n"
fi

echo -e "\nThe Depends field must have $(cat list-of-packages | grep FD | wc -l) packages.\n"
if [ "$IGNORE1" ]
then
    echo -e "\nThe following packages are marked in 'variable' file to be ignored to avoid a false positive. Please, read the file.\n"
    echo -e "$IGNORE1 $IGNORE2 $IGNORE3 $IGNORE4 \n"| sed 's/|/ /g'
fi

# FIXME
TEST=$(cat list-of-packages | grep FIXME | egrep "^[a-z0-9]")
echo -e "\nThe following packages are marked as \e[5mFIXME\e[25m:\n"
if [ "$TEST" ]
then
    cat list-of-packages | grep FIXME | egrep "^[a-z0-9]" | cut -d" " -f1
else
    echo "No packages at this moment."
fi
echo " "

# Remove 'build' directory if no '--debug' is set
[ -d build -a -z "$DEBUG" ] && rm -rf build

exit 0
