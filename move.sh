#!/bin/bash

# move.sh -- move control.NEW and *.README.Debian.NEW to debian/ directory
#
# This file is part of the forensics-all.
#
# Copyright 2018 Joao Eriberto Mota Filho <eriberto@debian.org>
#
# You can use this program under the BSD-3-Clause conditions.

####################
### Main program ###
####################

### Initial check. We are in right place?

[ -e control.NEW ] || { echo -e "\nI can't find control.NEW. Aborting."; exit 1; }
[ -e forensics-all.README.Debian.NEW -o "$1" = "-f" ] || \
     { echo -e "\nI can't find forensics-all.README.Debian.NEW. Aborting. Use -f to ignore."; exit 1; }
[ -e forensics-all-gui.README.Debian.NEW -o "$1" = "-f" ] || \
     { echo -e "\nI can't find forensics-all-gui.README.Debian.NEW. Aborting. Use -f to ignore."; exit 1; }

### Go!

mv control.NEW debian/control
[ -e forensics-all.README.Debian.NEW ] && mv forensics-all.README.Debian.NEW debian/forensics-all.README.Debian
[ -e forensics-all-gui.README.Debian.NEW ] && mv forensics-all-gui.README.Debian.NEW debian/forensics-all-gui.README.Debian

echo -e "\nFiles control.NEW and *.README.Debian.NEW were moved to \ndebian/ place and renamed to control and *.README.Debian.\n"

exit 0
